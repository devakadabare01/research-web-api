from fastapi import FastAPI, HTTPException, Body, Query
from fastapi.encoders import jsonable_encoder
from prophet import Prophet
import pandas as pd
import os
import pickle
from typing import Optional,List
from pydantic import BaseModel
from numpy import int64
import aiomysql

app = FastAPI()

# Path to the directory where models are stored
PRODUCT_MODEL_DIR = "D:/Research/Code/Web API/models/productWise/"
PRODUCTWISE_USER_MODEL_DIR = "D:/Research/Code/Web API/models/productWiseUser/"
PRODUCT_LIST_PATH = "D:/Research/Code/Web API/data/productList.csv"
# Dictionary to hold the loaded models
models = {}
user_models = {}

# Database connection details
DATABASE_CONFIG = {
    'host': 'database-1.ctcuuk2q4f8f.ap-south-1.rds.amazonaws.com',
    'port': 3306,
    'user': 'admin',
    'password': 'Lakpriya1996',
    'db': 'researchData',
}

class Product(BaseModel):
    productName: str
    price: float

class Bundle(BaseModel):
    products: List[str]
    price: float

class UserPredictionRequest(BaseModel):
    start_date: str
    end_date: str
    products: List[Product]
    bundles: List[Bundle]

# Function to sanitize product names by replacing spaces with underscores
def sanitize_name(name: str) -> str:
    return name.replace(" ", "_")

# Function to remove "model_" prefix from product names
def remove_model_prefix(data):
    for key, products in data.items():
        for product in products:
            product[0] = product[0].replace("model_", "")
    return data

# Function to load a model for a given product
def load_model(product_name):
    model_path = os.path.join(PRODUCT_MODEL_DIR, f"{product_name}.pkl")
    if os.path.exists(model_path):
        with open(model_path, "rb") as f:
            return pickle.load(f)
    else:
        raise FileNotFoundError(f"No model found for {product_name}")


# Function to load a user model for a given product
def load_user_model(product_name):
    model_path = os.path.join(PRODUCTWISE_USER_MODEL_DIR, f"{product_name}.pkl")
    if os.path.exists(model_path):
        with open(model_path, "rb") as f:
            return pickle.load(f)
    else:
        raise FileNotFoundError(f"No model found for {product_name}")

# Load all models at app startup
@app.on_event("startup")
def load_models():
    global models
    # List all product model files in the models directory
    for filename in os.listdir(PRODUCT_MODEL_DIR):
        if filename.endswith(".pkl"):
            product_name = filename[:-4]  # Remove the .pkl extension to get the product name
            models[product_name] = load_model(product_name)

@app.on_event("startup")
def load_user_models():
    global user_models
    # List all product user model files in the models directory
    for filename in os.listdir(PRODUCTWISE_USER_MODEL_DIR):
        if filename.endswith(".pkl"):
            product_name = filename[:-4]  # Remove the _model.pkl extension to get the product name
            user_models[product_name] = load_user_model(product_name)

@app.get("/predictions/products")
async def get_predictions(start_date: str, end_date: str, top_n: int = 5):
    
    try:
        start_date = pd.to_datetime(start_date)
        end_date = pd.to_datetime(end_date)
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid date format. Please use YYYY-MM-DD.")

    future = pd.DataFrame(pd.date_range(start=start_date, end=end_date, freq='D'), columns=['ds'])

    results = {}
    for product, model in models.items():
        forecast = model.predict(future)
        total_sales = forecast['yhat'].sum()
        results[product] = total_sales

    sorted_results = sorted(results.items(), key=lambda x: x[1], reverse=True)
    most_selling = sorted_results[:top_n]
    least_selling = sorted_results[-top_n:]

    final_result = jsonable_encoder({"most_selling": most_selling, "least_selling": least_selling}, custom_encoder={int64: int})
    final_result = remove_model_prefix(final_result)
    
    return final_result

def convert_numpy(obj):
    if isinstance(obj, int64):
        return int(obj)  # or use obj.item() to convert numpy types to native Python types
    raise TypeError

#
@app.post("/predictions/users")
async def get_user_predictions(request: UserPredictionRequest):
    try:
        start_date = pd.to_datetime(request.start_date)
        end_date = pd.to_datetime(request.end_date)
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid date format. Please use YYYY-MM-DD.")
    
    future = pd.DataFrame(pd.date_range(start=start_date, end=end_date, freq='D'), columns=['ds'])
    
    user_results = {}

    # Predict individual products
    for product in request.products:
        sanitized_product_name = sanitize_name(product.productName)
        product_name = sanitized_product_name + "_model"
        if product_name in user_models:
            model_dict = user_models[product_name]
            for user_id, model in model_dict.items():
                forecast = model.predict(future)
                forecast['will_buy'] = forecast['yhat'].apply(lambda x: int(x > 0.5))  # Adjust threshold as needed
                if forecast['will_buy'].sum() > 0:
                    if user_id not in user_results:
                        user_results[user_id] = {"products": [], "bundles": []}
                    user_results[user_id]["products"].append({"productName": product.productName, "price": product.price})
        else:
            if "error" not in user_results:
                user_results["error"] = []
            user_results["error"].append(f"No model available for {product.productName}")

    # Predict bundles
    for bundle in request.bundles:
        bundle_assigned = set()
        for product_name in bundle.products:
            sanitized_product_name = sanitize_name(product_name)
            product_name_with_model = sanitized_product_name + "_model"
            if product_name_with_model in user_models:
                model_dict = user_models[product_name_with_model]
                for user_id, model in model_dict.items():
                    forecast = model.predict(future)
                    forecast['will_buy'] = forecast['yhat'].apply(lambda x: int(x > 0.5))  # Adjust threshold as needed
                    if forecast['will_buy'].sum() > 0 and user_id not in bundle_assigned:
                        if user_id not in user_results:
                            user_results[user_id] = {"products": [], "bundles": []}
                        user_results[user_id]["bundles"].append({"products": bundle.products, "price": bundle.price})
                        bundle_assigned.add(user_id)

    final_result = jsonable_encoder(
        [{"userid": user_id, "products": user_data["products"], "bundles": user_data["bundles"]} 
         for user_id, user_data in user_results.items() if user_id != "error"],
        custom_encoder={int64: int}
    )
    
    return final_result

# New endpoint to search products from MySQL database
@app.get("/productSearch")
async def product_search(query: Optional[str] = Query(None, description="Search query for product names")):
    conn = await aiomysql.connect(**DATABASE_CONFIG)
    cursor = await conn.cursor(aiomysql.DictCursor)

    try:
        sql_query = """
        SELECT name, ppp.price 
        FROM Product P 
        JOIN (
            SELECT * FROM (
                SELECT productId, price 
                FROM ProductPrice PC 
                ORDER BY date
            ) AS pp 
            GROUP BY pp.productId
        ) AS ppp 
        ON ppp.productId = P.id 
        WHERE P.name LIKE LOWER(%s);
        """
        like_query = f"%{query}%" if query else "%"
        await cursor.execute(sql_query, (like_query,))
        result = await cursor.fetchall()
        
        if not result:
            raise HTTPException(status_code=404, detail="No products found matching the search query.")
        
        return result
    
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error executing query: {str(e)}")
    
    finally:
        await cursor.close()
        conn.close()

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
